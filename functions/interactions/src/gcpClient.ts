import {FlatCache} from "flat-cache";
import {
  InstancesClient,
  InstanceTemplatesClient,
  ZoneOperationsClient,
  ZonesClient,
} from "@google-cloud/compute";
import {google} from "@google-cloud/compute/build/protos/protos";
import {google as gax} from "google-gax/build/protos/operations";
import {logger} from "firebase-functions";
import {projectID} from "firebase-functions/params";

type ZoneFilterFunction = (item: google.cloud.compute.v1.IZone) => boolean | undefined;
type TemplateFilterFunction = (item: google.cloud.compute.v1.IInstanceTemplate) => boolean | undefined;
type InstanceFilterFunction = (item: google.cloud.compute.v1.IInstance) => boolean | undefined;

interface ListRequest {
  cacheKey: string;
  refreshFunction: CallableFunction;
  filter?: CallableFunction;
  forceRefresh?: boolean;
  maxResults?: number;
}

interface ListZonesRequest {
  filter?: ZoneFilterFunction;
  forceRefresh?: boolean;
}

interface ListTemplatesRequest {
  filter?: TemplateFilterFunction;
  forceRefresh?: boolean;
}

interface ListInstancesRequest {
  filter?: InstanceFilterFunction;
  forceRefresh?: boolean;
  queryFilter?: string;
}

interface GetInstanceRequest {
  name: string;
  zone?: string;
}

interface StartInstanceRequest {
  name: string;
  zone?: string;
}

interface StopInstanceRequest {
  name: string;
  zone?: string;
}

interface CreateInstanceRequest {
  name: string;
  template: string,
  zone: string;
  machineType?: string;
  labels?: {[key: string]: string};
}

interface DeleteInstanceRequest {
  name: string;
  zone?: string;
}

interface SetLabelsRequest {
  instance: string;
  labels: {[key: string]: string};
  labelFingerprint: string | undefined | null;
  zone?: string;
}

/**
 * A singleton client for interacting with Google Cloud Platform (GCP) services.
 * This client handles caching, resource fetching, and various operations on GCP resources.
 */
class GcpClient {
  private static instance: GcpClient;

  private instancesClient;
  private templatesClient;
  private operationsClient;
  private zonesClient;

  private cache: FlatCache;

  /**
   * Constructs a new GcpClient instance.
   */
  private constructor() {
    this.cache = new FlatCache({
      ttl: 60 * 1000, // 1 minute
      expirationInterval: 10 * 1000, // 10 seconds
    });
    this.instancesClient = new InstancesClient();
    this.templatesClient = new InstanceTemplatesClient();
    this.operationsClient = new ZoneOperationsClient();
    this.zonesClient = new ZonesClient();
  }

  /**
   * Gets the singleton instance of GcpClient.
   * @return {GcpClient} The singleton instance of GcpClient.
   */
  static getClient(): GcpClient {
    if (!GcpClient.instance) {
      GcpClient.instance = new GcpClient();
    }
    return GcpClient.instance;
  }

  /**
   * Generic get method to fetch resources from GCP.
   * Handles caching, refreshing, and local filtering.
   * @param {ListRequest} config - The configuration object for the list method.
   * @return {Promise<any>} A promise that resolves to the fetched resources.
   * @throws {Error} Throws an error if the request to fetch resources fails.
   */
  async #list(config: ListRequest): Promise<unknown> {
    const {cacheKey, refreshFunction, filter, forceRefresh = false, maxResults = 12} = config;
    let result: Array<unknown> | undefined;

    if (!forceRefresh) {
      result = this.cache.get(cacheKey) as Array<unknown>;
      if (result) {
        logger.debug(`${cacheKey}: ${result.length} (cached)`);
      }
    }

    if (!result) {
      try {
        result = await refreshFunction();
        this.cache.setKey(cacheKey, result);
        logger.debug(`${cacheKey}: ${result?.length} (refreshed)`);
      } catch (error) {
        logger.error(error);
        throw error;
      }
    }
    if (result) {
      if (filter) {
        result = result.filter((item) => filter(item));
      }
      if (maxResults) {
        result = result.slice(0, maxResults);
      }
    }
    return result;
  }

  /**
   * Waits for a specified operation to complete.
   * @param {string} operation - The name of the operation to wait for.
   * @param {string} zone - The zone of the operation.
   * @return {Promise<void>} A promise that resolves when the operation is complete.
   * @throws {Error} Throws an error if the operation fails.
   */
  async waitForOperation(operation: gax.longrunning.Operation, zone: string): Promise<void> {
    logger.debug(`Waiting for operation: ${operation.name}`);
    const [operationResponse] = await this.operationsClient.wait({
      operation: operation.name,
      project: projectID.value(),
      zone: zone,
    });

    if (operationResponse.error) {
      throw new Error(operationResponse.error?.errors?.[0]?.message || "Unknown error");
    }
  }
  /**
   * Infers the zone of an instance by its name.
   * @param {string} instanceName - The name of the instance.
   * @return {Promise<string | undefined>} A promise that resolves to the zone of the instance, or undefined if not found.
   */
  async inferZone(instanceName: string): Promise<string | undefined> {
    const instances = await this.listInstances({filter: (instance) => instance.name === instanceName});
    return instances[0]?.zone?.split("/").pop() || undefined;
  }

  /**
   * Lists the available zones for the specified project.
   * @param {ListZonesRequest} config - The configuration object for listing zones.
   * @return {Promise<google.cloud.compute.v1.IZone[]>} A promise that resolves to an array of zones.
   * @throws {Error} Throws an error if the request to list zones fails.
   */
  async listZones(config: ListZonesRequest = {}): Promise<google.cloud.compute.v1.IZone[]> {
    const {filter, forceRefresh = false} = config;
    const zoneRefreshFunction = async () => {
      const response = await this.zonesClient.list({project: projectID.value()});
      return response[0];
    };
    return await this.#list({cacheKey: "zones", refreshFunction: zoneRefreshFunction, filter, forceRefresh}) as google.cloud.compute.v1.IZone[];
  }

  /**
   * Lists Compute Instance Templates.
   * @param {ListTemplatesRequest} config - The configuration object for listing instance templates.
   * @return {Promise<google.cloud.compute.v1.IInstanceTemplate[]>} A promise that resolves to an array of instance templates.
   * @throws {Error} Throws an error if the request to list instance templates fails.
   */
  async listTemplates(config: ListTemplatesRequest = {}): Promise<google.cloud.compute.v1.IInstanceTemplate[]> {
    const {filter, forceRefresh = false} = config;
    const refreshFunction = async () => {
      const aggListRequest = this.templatesClient.aggregatedListAsync({
        project: projectID.value(),
        filter: "properties.labels.type=game",
      });
      const results = [];
      for await (const [, response] of aggListRequest) {
        if (response.instanceTemplates && response.instanceTemplates.length) {
          results.push(...response.instanceTemplates);
        }
      }
      return results;
    };
    return await this.#list({cacheKey: "templates", refreshFunction, filter, forceRefresh}) as google.cloud.compute.v1.IInstanceTemplate[];
  }

  /**
   * Gets a specific instance template by name.
   * @param {string} name - The name of the instance template to get.
   * @return {Promise<google.cloud.compute.v1.IInstanceTemplate>} A promise that resolves to the instance template.
   * @throws {Error} Throws an error if the request to get the instance template fails.
   */
  async getTemplate(name: string): Promise<google.cloud.compute.v1.IInstanceTemplate> {
    try {
      const [response] = await this.templatesClient.get({
        project: projectID.value(),
        instanceTemplate: name,
      });
      return response;
    } catch (error) {
      logger.error(error);
      throw error;
    }
  }

  /**
   * Lists Compute Instances.
   * @param {ListInstancesRequest} config - The configuration object for listing instances.
   * @return {Promise<google.cloud.compute.v1.IInstance[]>} A promise that resolves to an array of instances.
   * @throws {Error} Throws an error if the request to list instances fails.
   */
  async listInstances(config: ListInstancesRequest = {}): Promise<google.cloud.compute.v1.IInstance[]> {
    const {filter, forceRefresh = false, queryFilter = "labels.type=game"} = config;
    const refreshFunction = async () => {
      const aggListRequest = this.instancesClient.aggregatedListAsync({
        project: projectID.value(),
        filter: queryFilter,
      });
      const results = [];
      for await (const [, response] of aggListRequest) {
        if (response.instances && response.instances.length) {
          results.push(...response.instances);
        }
      }
      return results;
    };
    return await this.#list({cacheKey: "instances", refreshFunction, filter, forceRefresh}) as google.cloud.compute.v1.IInstance[];
  }

  /**
   * Gets a specific instance by name and zone.
   * @param {GetInstanceRequest} config - The configuration object for getting an instance.
   * @param {string} config.name - The name of the instance to get.
   * @param {string} config.zone - The zone of the instance.
   * @return {Promise<google.cloud.compute.v1.IInstance>} A promise that resolves to the instance.
   * @throws {Error} Throws an error if the request to get the instance fails.
   */
  async getInstance(config: GetInstanceRequest): Promise<google.cloud.compute.v1.IInstance> {
    const {name, zone} = config;
    if (!zone) {
      config.zone = await this.inferZone(name);
    }
    try {
      const [response] = await this.instancesClient.get({
        project: projectID.value(),
        zone: zone,
        instance: name,
      });
      return response;
    } catch (error) {
      logger.error(error);
      throw error;
    }
  }

  /**
   * Starts an instance.
   * @param {StartInstanceRequest} config - The configuration object for starting an instance.
   * @param {string} config.name - The name of the instance to start.
   * @param {string} [config.zone] - The zone of the instance. If not provided, it will be inferred.
   * @return {Promise<gax.longrunning.Operation>} A promise that resolves to the operation response.
   * @throws {Error} Throws an error if the request to start the instance fails.
   */
  async startInstance(config: StartInstanceRequest): Promise<gax.longrunning.Operation> {
    const {name, zone} = config;
    if (!zone) {
      config.zone = await this.inferZone(name);
    }
    try {
      logger.debug(`Starting instance: ${name}`);
      // Start the instance
      const [response] = await this.instancesClient.start({
        project: projectID.value(),
        zone: zone,
        instance: name,
      });
      return response.latestResponse;
    } catch (error) {
      logger.error(error);
      throw error;
    }
  }


  /**
   * Stops an instance.
   * @param {StopInstanceRequest} config - The configuration object for stopping an instance.
   * @param {string} config.name - The name of the instance to stop.
   * @param {string} [config.zone] - The zone of the instance. If not provided, it will be inferred.
   * @return {Promise<gax.longrunning.Operation>} A promise that resolves to the operation response.
   * @throws {Error} Throws an error if the request to stop the instance fails.
   */
  async stopInstance(config: StopInstanceRequest): Promise<gax.longrunning.Operation> {
    const {name, zone} = config;
    if (!zone) {
      config.zone = await this.inferZone(name);
    }
    try {
      logger.debug(`Stopping instance: ${name}`);
      // Stop the instance
      const [response] = await this.instancesClient.stop({
        project: projectID.value(),
        zone: zone,
        instance: name,
      });
      return response.latestResponse;
    } catch (error) {
      logger.error(error);
      throw error;
    }
  }


  /**
   * Creates an instance.
   * @param {CreateInstanceRequest} config - The configuration object for creating an instance.
   * @return {Promise<gax.longrunning.Operation>} A promise that resolves to the operation response.
   * @throws {Error} Throws an error if the request to create the instance fails.
   */
  async createInstance(config: CreateInstanceRequest): Promise<gax.longrunning.Operation> {
    const {name, template, zone, machineType, labels} = config;
    const instanceTemplate = await this.getTemplate(template);
    try {
      logger.debug(`Creating instance: ${name}`);
      // Create the instance
      const req: google.cloud.compute.v1.IInsertInstanceRequest = {
        project: projectID.value(),
        zone: zone,
        sourceInstanceTemplate: instanceTemplate.selfLink,
        instanceResource: {
          name: name,
          labels: {
            ...instanceTemplate.properties?.labels,
            ...labels,
          },
          machineType: (machineType) ? `zones/${zone}/machineTypes/${machineType}` : undefined,
        },
      };
      const [response] = await this.instancesClient.insert(req);
      return response.latestResponse;
    } catch (error) {
      logger.error(error);
      throw error;
    }
  }

  /**
   * Delete an instance.
   * @param {DeleteInstanceRequest} config - The configuration object for destroying an instance.
   * @return {Promise<gax.longrunning.Operation>} A promise that resolves to the operation response.
   * @throws {Error} Throws an error if the request to destroy the instance fails.
   */
  async deleteInstance(config: DeleteInstanceRequest): Promise<gax.longrunning.Operation> {
    const {name, zone} = config;
    if (!zone) {
      config.zone = await this.inferZone(name);
    }
    try {
      logger.debug(`Deleting instance: ${name}`);
      // Delete the instance
      const [response] = await this.instancesClient.delete({
        project: projectID.value(),
        zone: zone,
        instance: name,
      });
      return response.latestResponse;
    } catch (error) {
      logger.error(error);
      throw error;
    }
  }

  /**
   * Sets labels for a specific instance.
   * @param {SetLabelsRequest} config - The configuration object for setting labels.
   * @param {string} config.instance - The name of the instance.
   * @param {Object.<string, string>} config.labels - The labels to set.
   * @param {string} [config.zone] - The zone of the instance. If not provided, it will be inferred.
   * @return {Promise<gax.longrunning.Operation>} A promise that resolves to the operation response.
   * @throws {Error} Throws an error if the request to set labels fails.
   */
  async setLabels(config: SetLabelsRequest): Promise<gax.longrunning.Operation> {
    const {instance, labels, labelFingerprint, zone} = config;
    if (!zone) {
      config.zone = await this.inferZone(instance);
    }
    // Construct request
    const request: google.cloud.compute.v1.ISetLabelsInstanceRequest = {
      project: projectID.value(),
      instance: instance,
      zone: zone,
      instancesSetLabelsRequestResource: {
        labels: labels,
        labelFingerprint: labelFingerprint,
      },
    };

    // Run request
    const [response] = await this.instancesClient.setLabels(request);
    return response.latestResponse;
  }
}
export default GcpClient;
