// Imports
import {
  SlashCreator,
  GCFServer,
  SlashCommand,
} from "slash-create";
import {logger} from "firebase-functions";
import {defineString, defineSecret} from "firebase-functions/params";
import {onInit} from "firebase-functions/v2/core";
import {onRequest} from "firebase-functions/https";

import TemplatesCommand from "./commands/templates";
import ServersCommand from "./commands/servers";

// Configs
const discordApplicationId = defineString("GCLOUD_DISCORD_APPLICATION_ID");
const discordPublicKey = defineString("GCLOUD_DISCORD_PUBLIC_KEY");
const discordToken = defineSecret("GCLOUD_DISCORD_TOKEN");
const serviceAccountEmail = defineString("GCLOUD_SERVICE_ACCOUNT");

onInit(() => {
  // Create the SlashCreator instance
  const creator = new SlashCreator({
    applicationID: discordApplicationId.value(),
    publicKey: discordPublicKey.value(),
    token: discordToken.value(),
  });
  creator.on("debug", (message) => logger.debug(message));
  creator.on("warn", (message) => logger.warn(message));
  creator.on("error", (error: Error) => logger.error(error));
  creator.on("synced", () => logger.debug("Commands synced!"));
  creator.on("commandRegister", (command: SlashCommand) =>
    logger.debug(`Registered command ${command.commandName}`)
  );
  creator.on("commandError", (command: SlashCommand) =>
    logger.error(`Command ${command.commandName} failed`)
  );

  /**
   * Registers and syncs commands for the SlashCreator instance.
   */
  async function setupCommands() {
    creator.registerCommand(TemplatesCommand);
    creator.registerCommand(ServersCommand);
    await creator.syncCommands();
  }

  // Register commands and start the server
  creator.withServer(new GCFServer(module.exports, "interactions"));
  setupCommands();
});

exports.games_discord_interactions = onRequest(
  {
    cpu: 0.5,
    memory: "512MiB",
    secrets: [discordToken],
    serviceAccount: serviceAccountEmail,
    labels: {type: "game"},
  },
  async (req, res) => {
    await module.exports.interactions(req, res);
  }
);
