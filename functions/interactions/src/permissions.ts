import {CommandContext} from "slash-create";
import {logger} from "firebase-functions";

/**
 * Checks if the user has the required roles.
 * @param {CommandContext} ctx - The command context.
 * @param {string[]} roles - The roles to check against.
 * @return {Promise<boolean>} - Whether the user has the required roles
 */
export async function permissionCheck(ctx: CommandContext, roles: string[]) {
  const hasRequiredRoles =
    roles.length === 0 ||
    (ctx.member && ctx.member.roles.some((role) => roles.includes(role)));
  if (hasRequiredRoles) {
    logger.debug("Member has required roles.");
    return true;
  }
  logger.debug("Member does not have required roles.");
  await ctx.send("Permission denied. You do not have the required roles.");
  return false;
}
