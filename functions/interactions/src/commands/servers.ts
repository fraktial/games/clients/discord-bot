import {AutocompleteContext, CommandContext, SlashCommand, CommandOptionType, SlashCreator} from "slash-create";

import {permissionCheck} from "../permissions";
import GcpClient from "../gcpClient";
import {table} from "table";
import {logger} from "firebase-functions";

const gcpClient = GcpClient.getClient();


// Configs
const LIST_ROLES = process.env.LIST_ROLES ?
  process.env.LIST_ROLES.split(",") :
  [];
const START_STOP_ROLES = process.env.START_STOP_ROLES ?
  process.env.START_STOP_ROLES.split(",") :
  [];
const CREATE_DESTROY_ROLES = process.env.CREATE_DESTROY_ROLES ?
  process.env.CREATE_DESTROY_ROLES.split(",") :
  [];

/**
 * Command to manage game servers.
 */
class ServersCommand extends SlashCommand {
  /**
   * Constructor for the ServersCommand class.
   * @param {SlashCreator} creator - The creator instance.
   */
  constructor(creator: SlashCreator) {
    super(creator, {
      guildIDs: process.env.DISCORD_GUILD_ID,
      name: "servers",
      description: "Manage game cloud servers",
      options: [
        {
          name: "list",
          description: "List game servers",
          type: CommandOptionType.SUB_COMMAND,
        },
        {
          name: "start",
          description: "Start game server",
          type: CommandOptionType.SUB_COMMAND,
          options: [
            {
              name: "name",
              description: "Server name",
              type: CommandOptionType.STRING,
              required: true,
              autocomplete: true,
            },
            {
              name: "zone",
              description: "Instance zone",
              type: CommandOptionType.STRING,
              required: false,
              autocomplete: true,
            },
          ],
        },
        {
          name: "stop",
          description: "Stop Compute Instances",
          type: CommandOptionType.SUB_COMMAND,
          options: [
            {
              name: "name",
              description: "Instance name",
              type: CommandOptionType.STRING,
              required: true,
              autocomplete: true,
            },
            {
              name: "zone",
              description: "Instance zone",
              type: CommandOptionType.STRING,
              required: false,
              autocomplete: true,
            },
          ],
        },
        {
          name: "create",
          description: "Create a new game server",
          type: CommandOptionType.SUB_COMMAND,
          options: [
            {
              name: "name",
              description: "Server name",
              type: CommandOptionType.STRING,
              required: true,
            },
            {
              name: "template",
              description: "Template name",
              type: CommandOptionType.STRING,
              required: true,
              autocomplete: true,
            },
            {
              name: "zone",
              description: "Instance zone (check gcpping.com)",
              type: CommandOptionType.STRING,
              required: true,
              autocomplete: true,
            },
            {
              name: "machine",
              description: "Machine type",
              type: CommandOptionType.STRING,
              required: false,
            },
          ],
        },
        {
          name: "destroy",
          description: "Destroy a game server",
          type: CommandOptionType.SUB_COMMAND,
          options: [
            {
              name: "name",
              description: "Server name",
              type: CommandOptionType.STRING,
              required: true,
              autocomplete: true,
            },
            {
              name: "zone",
              description: "Instance zone",
              type: CommandOptionType.STRING,
              required: false,
              autocomplete: true,
            },
          ],
        },
      ],
    });
  }


  /**
   * Autocomplete fields in discord.
   * @param {AutocompleteContext} ctx - The autocomplete context.
   */
  async autocomplete(ctx: AutocompleteContext) {
    const input = ctx.options[ctx.subcommands[0]][ctx.focused];
    switch (ctx.focused) {
    case "zone": {
      const zones = await gcpClient.listZones({filter: (zone) => zone.name?.startsWith(input)});
      return zones
        .map((zone) => ({name: zone.name, value: zone.name}));
    }
    case "template": {
      const templates = await gcpClient.listTemplates({filter: (template) => template.name?.startsWith(input)});
      return templates
        .map((templates) => ({name: templates.name, value: templates.name}));
    }
    case "name": {
      const instances = await gcpClient.listInstances({filter: (instance) => instance.name?.startsWith(input)});
      return instances
        .map((instance) => ({name: instance.name, value: instance.name}));
    }
    default:
      return [];
    }
  }

  /**
 * Entrypoint for distcord interactions.
 * @param {CommandContext} ctx - The command context.
 */
  async run(ctx: CommandContext) {
    ctx.defer();
    switch (ctx.subcommands[0]) {
    case "list":
      if (await permissionCheck(ctx, LIST_ROLES)) {
        return await this.listServers();
      }
      break;
    case "start":
      if (await permissionCheck(ctx, START_STOP_ROLES)) {
        return await this.startServer(ctx);
      }
      break;
    case "stop":
      if (await permissionCheck(ctx, START_STOP_ROLES)) {
        return await this.stopServer(ctx);
      }
      break;
    case "create":
      if (await permissionCheck(ctx, CREATE_DESTROY_ROLES)) {
        return await this.createServer(ctx);
      }
      break;
    case "destroy":
      if (await permissionCheck(ctx, CREATE_DESTROY_ROLES)) {
        return await this.destroyServer(ctx);
      }
      break;
    }
    return await ctx.send("Unknown servers command");
  }

  /**
   * List game servers.
   * @param {CommandContext} ctx - The command context.
   */
  async listServers() {
    try {
      // List all instances with label type=game
      const instances = await gcpClient.listInstances({forceRefresh: true});

      // If no instances found, return
      if (instances.length === 0) {
        return "No instances found.";
      }

      // Prepare the data for the table
      const instanceData = [
        ["Name", "Zone", "Status", "External IP"],
        ...instances.map((instance) => [
          instance.name,
          instance.zone?.split("/").pop() || "unknown",
          instance.status || "unknown",
          instance.networkInterfaces?.[0]?.accessConfigs?.[0]?.natIP || "unknown",
        ]),
      ];

      // Create a table and send it to the channel
      return `\`\`\`${table(instanceData)}\`\`\``;
    } catch (error) {
      logger.error(error);
      return "Failed to list instances. Please try again later.";
    }
  }

  /**
   * Start a game server instance.
   * @param {CommandContext} ctx - The command context.
   */
  async startServer(ctx: CommandContext) {
    const name = ctx.options["start"].name;
    const zone = ctx.options["start"].zone || await gcpClient.inferZone(name);
    try {
      ctx.send(`\`\`\`Starting ${name}...\`\`\``);
      const op = await gcpClient.startInstance({name, zone});
      await gcpClient.waitForOperation(op, zone);
      const instance = await gcpClient.getInstance({name, zone});
      await gcpClient.setLabels({
        instance: name,
        labels: {...instance.labels, "discord-starter": ctx.user.id},
        labelFingerprint: instance.labelFingerprint,
        zone: zone,
      });
      return `\`\`\`Started ${name} with IP ${instance.networkInterfaces?.[0]?.accessConfigs?.[0]?.natIP}\`\`\``;
    } catch (error) {
      logger.error(error);
      return `\`\`\`${error}\`\`\``;
    }
  }

  /**
   * Stop a game server instance.
   * @param {CommandContext} ctx - The command context.
   */
  async stopServer(ctx: CommandContext) {
    const name = ctx.options["stop"].name;
    const zone = ctx.options["stop"].zone || await gcpClient.inferZone(name);
    try {
      ctx.send(`\`\`\`Stopping ${name}...\`\`\``);
      const op = await gcpClient.stopInstance({name, zone});
      await gcpClient.waitForOperation(op, zone);
      return `\`\`\`Stopped ${name}\`\`\``;
    } catch (error) {
      logger.error(error);
      return `\`\`\`${error}\`\`\``;
    }
  }

  /**
   * Create a game server instance.
   * @param {CommandContext} ctx - The command context.
   */
  async createServer(ctx: CommandContext) {
    const name = ctx.options["create"].name;
    const zone = ctx.options["create"].zone;
    const template = ctx.options["create"].template;
    const machine = ctx.options["create"].machine;

    try {
      ctx.send(`\`\`\`Creating ${name}...\`\`\``);
      const op = await gcpClient.createInstance({
        name: name,
        zone: zone,
        template: template,
        machineType: machine,
        labels: {"discord-creator": ctx.user.id, "discord-starter": ctx.user.id},
      });
      await gcpClient.waitForOperation(op, zone);
      const instance = await gcpClient.getInstance({name, zone});
      return `\`\`\`Created ${name} with IP ${instance.networkInterfaces?.[0]?.accessConfigs?.[0]?.natIP}\`\`\``;
    } catch (error) {
      logger.error(error);
      return `\`\`\`${error}\`\`\``;
    }
  }

  /**
   * Destroy a game server instance.
   * @param {CommandContext} ctx - The command context.
   */
  async destroyServer(ctx: CommandContext) {
    const name = ctx.options["destroy"].name;
    const zone = ctx.options["destroy"].zone || await gcpClient.inferZone(name);
    try {
      ctx.send(`\`\`\`Destroying ${name}...\`\`\``);
      const op = await gcpClient.deleteInstance({name, zone});
      await gcpClient.waitForOperation(op, zone);
      return `\`\`\`Destroyed ${name}\`\`\``;
    } catch (error) {
      logger.error(error);
      return `\`\`\`${error}\`\`\``;
    }
  }
}

export default ServersCommand;
