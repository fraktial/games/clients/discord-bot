import {BaseSlashCreator, CommandContext, SlashCommand, CommandOptionType} from "slash-create";
import {permissionCheck} from "../permissions";
import GcpClient from "../gcpClient";
import {table} from "table";
import {logger} from "firebase-functions";

const gcpClient = GcpClient.getClient();

const LIST_ROLES = process.env.LIST_ROLES ?
  process.env.LIST_ROLES.split(",") :
  [];

/**
 * Command to manage game server templates.
 */
class TemplatesCommand extends SlashCommand {
  /**
   * Constructor for TemplatesCommand.
   * @param {BaseSlashCreator} creator - The creator of the slash command.
   */
  constructor(creator: BaseSlashCreator) {
    super(creator, {
      guildIDs: process.env.DISCORD_GUILD_ID,
      name: "templates",
      description: "Manage game server templates",
      options: [
        {
          name: "list",
          description: "List available templates",
          type: CommandOptionType.SUB_COMMAND,
        },
      ],
    });
  }

  /**
   * Executes the command based on the provided context.
   * @param {CommandContext} ctx - The context of the command execution.
   * @return {Promise<void>} - A promise that resolves when the command execution is complete.
   * @remarks
   * This function defers the context and then processes the subcommands.
   * If the subcommand is "list" and the user has the required permissions, it lists the templates.
   * Otherwise, it sends an "Unknown command." message.
   */
  async run(ctx: CommandContext) {
    logger.debug(`Received command: ${ctx.commandName}`);
    ctx.defer();
    switch (ctx.subcommands[0]) {
    case "list":
      if (await permissionCheck(ctx, LIST_ROLES)) {
        return await this.listTemplates();
      }
    }
    return "Unknown command.";
  }

  /**
   * Lists all instance templates with label type=game and sends the result as a table.
   * @param {CommandContext} ctx - The context of the command execution.
   * @return {Promise<void>} - A promise that resolves when the templates are listed.
   */
  async listTemplates() {
    try {
      // List all instance templates
      const templates = await gcpClient.listTemplates({forceRefresh: true});

      // If no templates found, return
      if (templates.length === 0) {
        return "No templates found.";
      }

      // Prepare the data for the table
      const templatesData = [
        ["Template", "Default Machine"],
        ...templates.map((template) => [
          template.name,
          template.properties?.machineType ?? "unknown",
        ]),
      ];

      // Create a table and send it to the channel
      return `\`\`\`${table(templatesData)}\`\`\``;
    } catch (error) {
      logger.error(error);
      return `\`\`\`${error}\`\`\``;
    }
  }
}

export default TemplatesCommand;
