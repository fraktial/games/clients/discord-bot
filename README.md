# Discord Bot for GCP Compute Engine

## TODOs
[ ] helper function for long ops
[ ] helper functions for manging global vars (cach/lazyload) i.e. getOrSet(var) and getAndUpdate(var)
[ ] implement Winston logging for commands

## Usage
- `/templates list`
- `/servers list`
- `/servers stop NAME [ZONE]`
- `/servers start NAME [ZONE]`
- `/servers destroy NAME [ZONE]`
- `/servers create NAME ZONE TEMPLATE [MACHINETYPE]`


## Environment Variables
This Application requires the following variables at runtime:

  | Name                        | Value                                                                                                           |
  |-----------------------------|-----------------------------------------------------------------------------------------------------------------|
  | DISCORD_APPLICATION_ID      | The Application ID from the Discord developer portal (AKA Client ID)                                            |
  | DISCORD_PUBLIC_KEY          | The Public Key from the Discord developer portal                                                                |
  | DISCORD_TOKEN               | The Client Secret from the Discord developer portal                                                             |
  | DISCORD_GUILD_ID            | [optional, but strongly recommended] The ID of the discord server to register commands. 1 hour delay if not set |
  | GCLOUD_PROJECT              | The GCP Project ID                                                                                              |
  | DEFAULT_COMPUTE_ZONE        | [optional] The default zone to use for Compute Engine                                                           |

> If you face any problems, feel free to create an Issue from the Issues tab. I will try to respond as early as possible.
